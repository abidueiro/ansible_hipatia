#!/bin/bash

SERVER_LIST=$(ansible -i /etc/ansible/hosts hipatia --list-hosts)
##echo $SERVER_LIST
for host in $SERVER_LIST; do ssh-keyscan -H $host >> ~/.ssh/known_hosts; done
